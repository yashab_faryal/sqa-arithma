package de.rwth.swc.sqa.arithma;

class Token {
    private String value;
    private Type type;
    public enum Type {INTEGER, PLUS, MINUS, DIVIDE, MULTIPLY, MODULO, LPAREN, RPAREN, EOF};

    Token(String value, Type type) {
        this.value = value;
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public String getValue() {
        return value;
    }
}
