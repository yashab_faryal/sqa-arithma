package de.rwth.swc.sqa.arithma;

abstract class Operator extends Node {
    protected Node left;
    protected Node right;
}
