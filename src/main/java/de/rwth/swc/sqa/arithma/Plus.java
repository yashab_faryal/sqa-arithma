package de.rwth.swc.sqa.arithma;

class Plus extends Operator {

    Plus(Node left, Node right) {
        this.left = left;
        this.right = right;
    }

    public int evaluate() {
        return left.evaluate() + right.evaluate();
    }
}
