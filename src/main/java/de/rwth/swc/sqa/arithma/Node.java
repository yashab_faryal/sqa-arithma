package de.rwth.swc.sqa.arithma;

abstract class Node {
    abstract public int evaluate();
}
