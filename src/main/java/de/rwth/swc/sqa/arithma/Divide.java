package de.rwth.swc.sqa.arithma;

class Divide extends Operator {

    Divide(Node left, Node right) {
        this.left = left;
        this.right = right;
    }


    public int evaluate() {
        return left.evaluate() / right.evaluate();
    }
}
