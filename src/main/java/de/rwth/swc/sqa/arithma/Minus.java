package de.rwth.swc.sqa.arithma;

class Minus extends Operator {

    Minus(Node left, Node right) {
        this.left = left;
        this.right = right;
    }

    public int evaluate() {
        return left.evaluate() - right.evaluate();
    }
}
