package de.rwth.swc.sqa.arithmb;

import java.util.*;

class MathParser {
  private String expression;
  private ExpressionTree tree;
  private Node root, currentNode;
  public static final Map<String, Operator> OPERATORS;

  static {
    Map<String, Operator> map = new HashMap<String, Operator>();
    map.put("+", new Addition());
    map.put("-", new Subtraction());
    map.put("*", new Multiplication());
    map.put("/", new Division());
    map.put("%", new Modulo());
    OPERATORS = Collections.unmodifiableMap(map);
  }

  MathParser(String expression) {
    this.expression = expression;
    this.root = new Node();
    this.tree = new ExpressionTree(root);
    this.currentNode = root;
  }


  // Unless the passed number is empty, the number is set as the value of the current node and the
  // parent of this node is now considered to be the current node.
  public String checkNumber(String number) {
    if (!number.isEmpty()) {
      currentNode.setValue(Integer.parseInt(number));
      currentNode = currentNode.getParentNode();
    }
    return "";
  }

  public int eval() {
    String number = "";

    for (int i = 0; i < expression.length(); i++) {
      //Ignore whitespaces
      if (expression.charAt(i) == ' ') {
        continue;
      }

      //Create a new node
      Node newNode = new Node();
      // There is a root node and a current node, if the current node is null, create a new node, add the root as its
      //left child. If not, do nothing.
      fixTree();


      if (expression.charAt(i) == '(') {
        number = checkNumber(number);
        currentNode.setLeftNode(newNode);
        currentNode = newNode;
      } else if (expression.charAt(i) == ')') {
        number = checkNumber(number);
        currentNode = currentNode.getParentNode();
      } else if (expression.charAt(i) == '+' || expression.charAt(i) == '-'
          || expression.charAt(i) == '*' || expression.charAt(i) == '/'
          || expression.charAt(i) == '%') {
        number = checkNumber(number);
        Operator op = OPERATORS.get("" + expression.charAt(i));
        currentNode.setOperator(op);
        currentNode.setRightNode(newNode);
        currentNode = newNode;
      } else {
        number += expression.charAt(i);
      }
    }

    return tree.calculate();
  }

  //If the current node is null, a new node is created. This new node is now the root of the tree and the old tree root becomes the
  //left child of the new root.
  private void fixTree() {
    if (currentNode == null) {
      Node newRoot = new Node();
      newRoot.setLeftNode(tree.getRoot());
      tree.setRoot(newRoot);
      currentNode = newRoot;
    }
  }

}
