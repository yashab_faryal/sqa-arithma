package de.rwth.swc.sqa.arithmb;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MathParserTest {

    @ParameterizedTest
    @CsvSource({"(1*(2+1)), 3","1+3,4","(0*0),0","(1%1),0", "(1-8),-7","(3/2),1","(1+(6/2)),4","(-2+3),1","(2*1),2","(8/2),4"})
    public void evalTest(String expression, int expected){
        MathParser mathParser = new MathParser(expression);

        assertEquals(expected,mathParser.eval());
    }
}
