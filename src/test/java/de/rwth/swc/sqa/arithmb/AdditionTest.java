package de.rwth.swc.sqa.arithmb;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AdditionTest {


    Addition add = new Addition();


    @ParameterizedTest
    @CsvSource({"2,3", "3,-2", "-1,-2" ,"-3,0","3,0"})
    public void testParameters(int left, int right) {
        assertEquals(left+right,add.evaluate(left,right));
    }


}
