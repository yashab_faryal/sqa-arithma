package de.rwth.swc.sqa.arithmb;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DivisionTest {
    Division div = new Division();


    @Test
    public void testEvaluateException() {

        assertThrows(ArithmeticException.class, () -> div.evaluate(1, 0), "Test failed for division by zero");

    }

    @ParameterizedTest
    @CsvSource({"2,3", "3,-2", "-1,-2"})
    public void testParameters(int left, int right) {
        assertEquals((left/right),div.evaluate(left,right));
    }
}
