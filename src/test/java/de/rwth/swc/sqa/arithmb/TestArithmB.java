package de.rwth.swc.sqa.arithmb;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestArithmB {

    @Test
    void demonstrationTest() {
        final String expression = "(20 + 18)";
        final MathParser parser = new MathParser(expression);

        assertEquals(38, parser.eval());
    }
}
