package de.rwth.swc.sqa.arithmb;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ModuloTest {
    Modulo mod = new Modulo();


    @Test
    public void testEvaluateException() {

        assertThrows(ArithmeticException.class, () -> mod.evaluate(1, 0), "Test failed for division by zero");

    }

    @ParameterizedTest
    @CsvSource({"2,3", "3,-2", "-1,-2"})
    public void testParameters(int left, int right) {
        assertEquals((left%right),mod.evaluate(left,right));
    }
}
