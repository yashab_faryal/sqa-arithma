package de.rwth.swc.sqa.arithma;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

public class ParserTest {

    @ParameterizedTest(name = "expression {0} should evaluate to {1}")
    @CsvSource({
            "1 * 2 + 1 - 1, 2",
            "785 / 6 - (1 + (20 % 2) * 45), 129",
            "(0 * (0)), 0",
            "(1 % 1), 0",
    })
    void testValid(String expression, int result) {
        Node n = assertDoesNotThrow(() -> new Parser(new Lexer(expression)).parse());
        assertEquals(result, n.evaluate());
    }

    @ParameterizedTest(name = "expression {0} should evaluate to {1}")
    @CsvSource({
            "(-2 + 3), 1",
            "(2 + -3), -1",
            "(2 + (-3)), -1",
    })
    void testNegativeNumbers(String expression, int result) {
        Node n = assertDoesNotThrow(() -> new Parser(new Lexer(expression)).parse());
        assertEquals(result, n.evaluate());
    }

    @ParameterizedTest(name = "expression {0} should throw runtime exception")
    @CsvSource({
            "(0 + (10(0) - (67 / 12 * 34))",
            "1.0 - 2.4 * 3.567"
    })
    void testInvalid(String expression) {
        final Parser parser = new Parser(new Lexer(expression));
        assertThrows(RuntimeException.class, parser::parse, "Test passed for invalid expression");
    }
}
