package de.rwth.swc.sqa.arithma;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DivideTest {

    @ParameterizedTest(name = "{0} / {1} should be {2}")
    @CsvSource({
            "2, 3, 0",
            "30, -20, -1",
            "-123, -267, 0",
            "-25670, 3232, -7",
            "0, 1, 0",
    })
    public void testValid(int left, int right, int result) {
        Divide divide = new Divide(new Operand(left), new Operand(right));
        assertEquals(result, divide.evaluate());
    }

    @ParameterizedTest(name = "{0} / {1} should be {2}")
    @CsvSource({"-2147483648, -1, -2147483648"})
    /*
    On overflow, it goes back to the minimum value and continues from there.
     */
    public void testOverflow(int left, int right, int result) {
        Divide divide = new Divide(new Operand(left), new Operand(right));
        assertEquals(result, divide.evaluate());
    }


    @ParameterizedTest(name = "{0} / {1} should throw arithmetic exception")
    @CsvSource({"1, 0"})
    public void testDivideByZero(int left, int right) {
        Divide divide = new Divide(new Operand(left), new Operand(right));
        assertThrows(ArithmeticException.class, divide::evaluate, "Test passed for division by zero");
    }
}
