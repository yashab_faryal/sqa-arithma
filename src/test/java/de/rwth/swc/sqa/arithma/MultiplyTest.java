package de.rwth.swc.sqa.arithma;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MultiplyTest extends BaseTest {

    @ParameterizedTest(name = "{0} x {1} should be {2}")
    @CsvSource({
            "2, 3, 6",
            "30, -20, -600",
            "-123, -267, 32841",
            "-2567, 3232, -8296544",
            "0, 1, 0",
            "1, 0, 0"
    })
    public void testValid(int left, int right, int result) {
        Multiply multiply = new Multiply(new Operand(left), new Operand(right));
        assertEquals(result, multiply.evaluate());
    }

    @ParameterizedTest(name = "{0} x {1} should be {2}")
    @CsvSource({"-2147483648, -1, -2147483648"})
    /*
    On overflow, it goes back to the minimum value and continues from there.
     */
    public void testOverflow(int left, int right, int result) {
        Multiply multiply = new Multiply(new Operand(left), new Operand(right));
        assertEquals(result, multiply.evaluate());
    }

    @ParameterizedTest(name = "{0} x {1} should be {2}")
    @CsvSource({"-1073741825, 2, 2147483646"})
    /*
    On underflow, it goes back to the maximum value and continues from there.
     */
    public void testUnderflow(int left, int right, int result) {
        Multiply multiply = new Multiply(new Operand(left), new Operand(right));
        assertEquals(result, multiply.evaluate());
    }
}
