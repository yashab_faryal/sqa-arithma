package de.rwth.swc.sqa.arithma;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ModuloTest extends BaseTest {

    @ParameterizedTest(name = "{0} % {1} should be {2}")
    @CsvSource({
            "2, 3, 2",
            "30, -20, 10",
            "-123, -267, -123",
            "-25670, 3232, -3046",
            "0, 1, 0",
            "-2147483648, -1, 0"
    })
    public void testValid(int left, int right, int result) {
        Modulo modulo = new Modulo(new Operand(left), new Operand(right));
        assertEquals(result, modulo.evaluate());
    }

    @ParameterizedTest(name = "{0} % {1} should throw arithmetic exception")
    @CsvSource({"1, 0"})
    public void testModuloByZero(int left, int right) {
        Modulo modulo = new Modulo(new Operand(left), new Operand(right));
        assertThrows(ArithmeticException.class, modulo::evaluate, "Test passed for mod by zero");
    }
}
