package de.rwth.swc.sqa.arithma;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestArithmA {


    @Test
    void demonstrationTest() {
        final String expression = "(20 + 18)";

        final Lexer lex = new Lexer(expression);
        final Parser p = new Parser(lex);
        final Node n = p.parse();

        assertEquals(38, n.evaluate());

        /*
        1. should support expressions with integers, zero, +- integers
        2. should not support floating-point numbers
        3. should support:
            a. addition
            b. subtraction
            c. multiplication
            d. division
            e. modulo
            f. parentheses
            g. operator precedence must be maintained

         */
    }
}
