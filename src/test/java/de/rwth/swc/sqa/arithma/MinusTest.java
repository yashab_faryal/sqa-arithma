package de.rwth.swc.sqa.arithma;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MinusTest extends BaseTest {

    @ParameterizedTest(name = "{0} - {1} should be {2}")
    @CsvSource({
            "2, 3, -1",
            "30, -20, 50",
            "-123, -267, 144",
            "-2567, 3232, -5799",
            "0, 1, -1",
            "1, 0, 1"
    })
    public void testValid(int left, int right, int result) {
        Minus minus = new Minus(new Operand(left), new Operand(right));
        assertEquals(result, minus.evaluate());
    }

    @ParameterizedTest(name = "{0} - {1} should be {2}")
    @CsvSource({"2147483647, -1, -2147483648"})
    /*
    On overflow, it goes back to the minimum value and continues from there.
     */
    public void testOverflow(int left, int right, int result) {
        Minus minus = new Minus(new Operand(left), new Operand(right));
        assertEquals(result, minus.evaluate());
    }

    @ParameterizedTest(name = "{0} - {1} should be {2}")
    @CsvSource({"-2147483648, 1, 2147483647"})
    /*
    On underflow, it goes back to the maximum value and continues from there.
     */
    public void testUnderflow(int left, int right, int result) {
        Minus minus = new Minus(new Operand(left), new Operand(right));
        assertEquals(result, minus.evaluate());
    }
}
