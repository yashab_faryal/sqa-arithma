package de.rwth.swc.sqa.arithma;

public abstract class BaseTest {
    abstract void testValid(int left, int right, int result);

    void testOverflow(int left, int right, int result) {
    }

    void testUnderflow(int left, int right, int result) {
    }
}
